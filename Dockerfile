# Set the base image to use for the container
FROM unrealengine:latest

# Copy the project files into the container
COPY testhtml5 /app

# Set the working directory
WORKDIR /app

# Build the HTML5 target
RUN ue4-docker build -rhi=OpenGL -targetplatform=HTML5 -progress -verbose MyProject2.uproject